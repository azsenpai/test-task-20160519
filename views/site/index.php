<?php

$errors = get_value($params['errors']);

?>

<?php if ($errors): ?>
<ul class="list errors">
    <?php foreach ($errors as $error): ?>
    <li><?= $error ?></li>
    <?php endforeach ?>
</ul>
<?php endif ?>

<form method="post" enctype="multipart/form-data">
    <p>
        <label class="input-label" for="edit-full_name">ФИО</label>
        <br>
        <input class="input" name="full_name" id="edit-full_name" value="<?= $params['full_name'] ?>">
    <p>
    <p>
        <label class="input-label" for="edit-message">Текст сообщения</label>
        <br>
        <textarea class="input" name="message" id="edit-message"><?= $params['message'] ?></textarea>
    <p>
    <p>
        <label class="input-label" for="edit-image">Изображение</label>
        <br>
        <input type="file" name="image" id="edit-image">
    <p>
    <p>
        <input type="submit" value="Отправить">
    <p>
</form>

<a href="/comments">Все комментарии</a>