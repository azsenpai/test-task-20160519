<ul class="list">
    <li>ФИО: <?= $params['comment']['full_name'] ?></li>
    <li>Текст сообщения:<br> <?= nl2br($params['comment']['message']) ?></li>
    <?php if ($params['comment']['image_url']): ?>
    <li>Изображение:<br> <img class="comment-img" alt="" src="/<?= $params['comment']['image_url'] ?>"></li>
    <?php endif ?>
</ul>
<a href="/">Главная<a>