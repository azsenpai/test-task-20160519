<?php if ($params['comments']): ?>
    <table class="table-comments">
        <colgroup>
            <col span="1" class="col-id">
            <col span="1" class="col-full_name">
            <col span="1" class="col-message">
            <col span="1" class="col-image_url">
            <col span="1" class="col-created_at">
        </colgroup>
        <thead>
            <tr>
                <th>ID</th>
                <th>ФИО</th>
                <th>Текст сообщения</th>
                <th>Изображение</th>
                <th>Дата</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($params['comments'] as $comment): ?>
            <tr>
                <td><a href="/comment?comment_id=<?= $comment['id'] ?>"><?= $comment['id'] ?></a></td>
                <td><?= safe_value($comment['full_name']) ?></td>
                <td><?= safe_value($comment['message']) ?></td>
                <td><?php if ($comment['image_url']): ?><img class="comment-img-thumb" alt="" src="/<?= $comment['image_url'] ?>"><?php endif ?></td>
                <td><?= $comment['created_at'] ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php else: ?>
    <p>
        <b>Нет комментариев</b>
    </p>
<?php endif ?>

<a href="/">Главная<a>