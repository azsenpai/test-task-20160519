CREATE TABLE `comments` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `full_name` VARCHAR(50) NOT NULL DEFAULT '0',
    `message` TEXT NOT NULL,
    `image_url` VARCHAR(255) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;
