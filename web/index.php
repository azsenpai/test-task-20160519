<?php

require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'config', 'bootstrap.php']));

$config = require(path_join(BASE_DIR, 'config', 'web.php'));

require(path_join(BASE_DIR, 'controllers', 'site.php'));
require(path_join(BASE_DIR, 'models', 'site.php'));

$uri = get_uri();

switch ($uri) {
    case '':
        print site_index();
        break;

    case 'comment':
        $comment_id = (int)get('comment_id');

        print site_comment($comment_id);
        break;

    case 'comments':
        print site_comments();
        break;

    default:
        print not_found();
}
