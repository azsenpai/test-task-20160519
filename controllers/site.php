<?php

function site_index()
{
    $params = [
        'full_name' => trim(post('full_name')),
        'message' => trim(post('message')),
    ];

    if (is_post()) {
        if (empty($params['full_name'])) {
            $params['errors'][] = 'Поле "ФИО" обязательно для заполнения';
        }
        if (mb_strlen($params['message'], 'utf-8') < 10) {
            $params['errors'][] = 'Поле "Текст сообщения" обязательно для заполнения и должно быть не короче 10 символов';
        }

        if (empty($params['errors'])) {
            $image_url = '';

            if ($_FILES['image']['error'] === UPLOAD_ERR_OK && is_image($_FILES['image']['tmp_name'])) {
                $uploadfile = path_join(UPLOAD_DIR, $_FILES['image']['name']);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                    $image_url = UPLOAD_URL . '/' . $_FILES['image']['name'];
                }
            }

            $comment_id = add_comment($params['full_name'], $params['message'], $image_url);

            redirect('/comment?comment_id=' . $comment_id);
        }
    }

    print render('site/index', $params);
}

function site_comment($comment_id)
{
    $comment = get_comment($comment_id);

    if (!$comment) {
        redirect();
    }

    print render('site/comment', ['comment' => $comment]);
}

function site_comments()
{
    $comments = get_comments();
    print render('site/comments', ['comments' => $comments]);
}

function not_found()
{
    print render('site/error');
}
