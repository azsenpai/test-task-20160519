<?php

return [
    'layout' => path_join(BASE_DIR, 'views', 'layouts', 'main.php'),
    'db' => [
        'dsn' => 'mysql:host=localhost;dbname=test_task',
        'username' => 'root',
        'password' => '',
    ],
];
