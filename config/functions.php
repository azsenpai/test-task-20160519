<?php

function get_value(&$var, $default = null)
{
    return isset($var) ? $var : $default;
}

function safe_value($value)
{
    return htmlspecialchars($value);
}

function norm_path($path)
{
    return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
}

function path_join()
{
    return implode(DIRECTORY_SEPARATOR, func_get_args());
}

function get_uri($with_params = false)
{
    $uri = trim($_SERVER['REQUEST_URI'], '/');

    if (!$with_params) {
        $pos = strpos($uri, '?');
        $pos = ($pos === false) ? strlen($uri) : $pos;

        $uri = substr($uri, 0, $pos);
    }

    return $uri;
}

function get_view_content($view, $params)
{
    ob_start();

    require(path_join(BASE_DIR, 'views', norm_path($view) . '.php'));

    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function render($view, $params = [])
{
    global $config;

    ob_start();

    $content = get_view_content($view, $params);
    require($config['layout']);

    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function is_get()
{
    return strtoupper($_SERVER['REQUEST_METHOD']) == 'GET';
}

function is_post()
{
    return strtoupper($_SERVER['REQUEST_METHOD']) == 'POST';
}

function get($key, $default = null)
{
    return get_value($_GET[$key], $default);
}

function post($key, $default = null)
{
    return get_value($_POST[$key], $default);
}

function redirect($url = '/', $permanent = false)
{
    if (headers_sent() === false) {
        header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
    }

    exit();
}

function is_image($filename)
{
    $allowed_types = [
        IMAGETYPE_JPEG,
        IMAGETYPE_PNG,
        IMAGETYPE_GIF,
    ];

    $mimetype = exif_imagetype($filename);

    return ($mimetype !== false) && in_array($mimetype, $allowed_types) !== false;
}

function open_db()
{
    global $config;

    $dbh = new PDO($config['db']['dsn'], $config['db']['username'], $config['db']['password']);
    return $dbh;
}

function close_db($dbh)
{
    $dbh = null;
}
