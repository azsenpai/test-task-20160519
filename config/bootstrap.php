<?php

define('BASE_DIR', dirname(__DIR__));

require('functions.php');

define('WEB_DIR', path_join(BASE_DIR, 'web'));

define('UPLOAD_DIR', path_join(WEB_DIR, 'uploads'));
define('UPLOAD_URL', 'uploads');
